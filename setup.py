#!/usr/bin/env python

from distutils.core import setup

setup(
    name='pso',
    description='Particle Swarm Optimization in Python',
    version='0.2',
    url='https://code.siemens.com/markus.kaiser/python-pso',

    author='Markus Kaiser',
    author_email='markus.kaiser@siemens.com',

    packages=['pso'],
    install_requires = [
        'numpy',
    ]
)
