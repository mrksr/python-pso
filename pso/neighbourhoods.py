import itertools as it
import numpy as np


def ring_neighbourhood(num_particles, num_neighbours=5):
    # Python2 compatibility
    # In Python2 the usual zip returns a list, not a generator. Therefore, we
    # shadow it with the izip from itertools. In Python3, izip does not exist
    # anymore, but the usual zip returns a generator.
    try:
        from itertools import izip as zip
    except ImportError:
        global zip

    # See itertools recipies
    def consume(iterator, n):
        "Advance the iterator n-steps ahead. If n is none, consume entirely."
        # Use functions that consume iterators at C speed.
        if n is None:
            # feed the entire iterator into a zero-length deque
            collections.deque(iterator, maxlen=0)
        else:
            # advance to the empty slice starting at position n
            next(it.islice(iterator, n, n), None)

    num_neighbours = np.maximum(1, num_neighbours)
    # How many of them are left of the item itself
    left_side = (num_neighbours - 1) // 2
    # List of all num_particles
    ring = list(range(num_particles))
    # Rotate to the first position
    ring = ring[-left_side:] + ring[:-left_side]
    # Create tuple of iterators
    neighbours = it.tee(it.cycle(ring), num_neighbours)
    # advance the iterators appropriately
    for i in range(num_neighbours):
        consume(neighbours[i], i)

    # sample for every particle
    neighbourhoods = it.islice(zip(*neighbours), 0, num_particles)

    # We can return an array here, since the neighbourhood is rectangular.
    return np.array(list(neighbourhoods))


def full_neighbourhood(num_particles):
    return np.ones((num_particles, num_particles), dtype=np.bool)
