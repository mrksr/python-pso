from functools import partial, wraps
import operator
import numpy as np


# The functions here cannot be closures because of multiprocessing
def _func_with_args(func, args, kwargs, x):
    return func(x, *args, **kwargs)


def _map_rowwise_pool(pool, f, x):
    return np.array(pool.map(f, x))
def _map_1d_pool(pool, f, x):
    return np.array(pool.map(f, x))


def _map_rowwise_single(f, x):
    return np.apply_along_axis(f, 1, x)
def _map_1d_single(f, x):
    return np.array(list(map(f, x)))


def _best_social_p(best_index, bestfs, bestps, ns):
    best = best_index(bestfs[ns])
    return bestps[ns[best], :]


def _optimize_pso(
        func, bounds,
        improvement_comparison=operator.lt, best_index=np.nanargmin,
        num_particles=None, neighbourhoods=None,
        omega=0.72981, gamma_cognitive=1.49618, gamma_social=1.49618,
        velocity_clamp_factor=0.1,
        initial_positions=None, initial_velocities=None,
        max_iter=200, max_iter_no_improve=20,
        min_stepsize=1e-8, min_relative_improvement=1e-8,
        verbose=False, verbose_show_variables=False,
        func_is_vectorized=False,
        pool=None, num_processes=1,
        args=(), kwargs={}):
    func_with_args = partial(_func_with_args, func, args, kwargs)

    if verbose:
        verbose_print = print
    else:
        def verbose_print(*args):
            pass

    verbose_print('--- PSO')

    if num_particles is None:
        # Default to a linear amount of particles per dimension
        num_particles = max(1, len(bounds) * 50)

    if neighbourhoods is None:
        from .neighbourhoods import ring_neighbourhood
        verbose_print('No neighbourhood specified, falling back to ring_neighbourhood')
        neighbourhoods = ring_neighbourhood(num_particles, num_neighbours=num_particles//10)
    else:
        assert len(neighbourhoods) == num_particles, 'Every particle must have a neighbourhood'

    if pool is not None:
        verbose_print('Using supplied pool for multiprocessing')
        map_rowwise = partial(_map_rowwise_pool, pool)
        map_1d = partial(_map_1d_pool, pool)
    elif num_processes > 1:
        verbose_print(f'Using {num_processes} processes for optimization, creating new pool.')
        import multiprocessing as mp
        pool = mp.Pool(num_processes)
        map_rowwise = partial(_map_rowwise_pool, pool)
        map_1d = partial(_map_1d_pool, pool)
    else:
        map_rowwise = _map_rowwise_single
        map_1d = _map_1d_single

    bounds = np.array(bounds)
    lower_bounds, upper_bounds = bounds[:, 0], bounds[:, 1]
    maximum_velocity = velocity_clamp_factor * np.abs(upper_bounds - lower_bounds)
    assert np.all(lower_bounds <= upper_bounds), 'The bounds for all variables must be non-empty'

    #
    # Initialization
    #
    N = num_particles
    D = lower_bounds.shape[0]

    # Positions
    ps = np.random.uniform(size=(N, D))
    ps = ps * (upper_bounds - lower_bounds) + lower_bounds
    if initial_positions is not None:
        assert initial_positions.shape[1] == D, "Initial positions must have correct dimensionality"
        n_overwrite = np.minimum(N, initial_positions.shape[0])
        ps[:n_overwrite, :] = initial_positions[:n_overwrite, :]
        ps = np.clip(ps, lower_bounds, upper_bounds)
    # Velocities
    # Initialize with velocity 0
    vs = np.zeros_like(ps)
    if initial_velocities is not None:
        assert initial_velocities.shape[1] == D, "Initial velocities must have correct dimensionality"
        n_overwrite = np.minimum(N, initial_velocities.shape[0])
        vs[:n_overwrite] = initial_velocities[:n_overwrite]
        vs = np.clip(vs, -maximum_velocity, maximum_velocity)

    # Best positions
    bestps = np.empty_like(ps)
    # Best function values
    bestfs = np.empty(N)
    # Global best
    bestp = np.empty(D)
    bestf = None

    #
    # Iterations
    #
    iterations_with_no_improvement = 0
    for it in range(max_iter):
        # Update objective values
        if func_is_vectorized:
            fs = func_with_args(ps)
        else:
            fs = map_rowwise(func_with_args, ps)

        # Update best
        if bestf is None:
            # Initial iteration
            bestps[...] = ps[...]
            bestfs[...] = fs[...]
        else:
            better = improvement_comparison(fs, bestfs)
            bestps[better] = ps[better]
            bestfs[better] = fs[better]

        best_index_this = best_index(bestfs)
        bestp_this = bestps[best_index_this]
        bestf_this = bestfs[best_index_this]
        if bestf is None:
            bestp = bestp_this.copy()
            bestf = bestf_this.copy()
        elif improvement_comparison(bestf_this, bestf):
            if verbose_show_variables:
                verbose_print(f'New global best at iteration {it}: {bestf_this} at {bestp_this}')
            else:
                verbose_print(f'New global best at iteration {it}: {bestf_this}')

            # Check for early stops
            relative_improvement_amount = np.abs(bestf_this / bestf - 1.)
            # verbose_print('Relative Improvement: {}'.format(relative_improvement_amount))
            distance_to_previous_best = np.linalg.norm(bestp - bestp_this)
            # verbose_print('Distance: {}'.format(distance_to_previous_best))
            if relative_improvement_amount < min_relative_improvement:
                verbose_print('Improvement smaller than min_relative_improvement, stopping pso.')
                break

            if distance_to_previous_best < min_stepsize:
                verbose_print('Stepsize smaller than min_stepsize, stopping pso.')
                break

            # Update best
            bestp = bestp_this.copy()
            bestf = bestf_this.copy()

            iterations_with_no_improvement = 0
        else:
            iterations_with_no_improvement += 1
            if iterations_with_no_improvement > max_iter_no_improve:
                verbose_print('More than max_iter_no_improve iterations without improvement, stopping pso.')
                break


        # Update particles
        rc = np.random.uniform(size=(N, D))
        rs = np.random.uniform(size=(N, D))

        best_social_p = partial(_best_social_p, best_index, bestfs, bestps)
        # NOTE: Parallelizing this might do more harm than good because bestfs
        # and bestps might have to be copied.
        best_social_ps = map_1d(best_social_p, neighbourhoods)

        cognitive_component = gamma_cognitive * rc * (bestps - ps)
        social_component = gamma_social * rs * (best_social_ps - ps)
        vs = omega * vs + cognitive_component + social_component
        vs = np.clip(vs, -maximum_velocity, maximum_velocity)
        ps = np.clip(ps + vs, lower_bounds, upper_bounds)

        # Done for this iteration
        if verbose_show_variables:
            verbose_print(f'Best after iteration {it}: {bestf_this} at {bestp_this}')
        else:
            verbose_print(f'Best after iteration {it}: {bestf_this}')
    else:
        # There was no break
        verbose_print('Max_iter reached, stopping pso.')
    return bestp, bestf


optimize = _optimize_pso
minimize = _optimize_pso


@wraps(_optimize_pso)
def maximize(*args, **kwargs):
    return _optimize_pso(
        *args,
        improvement_comparison=operator.gt,
        best_index=np.nanargmax,
        **kwargs
    )
