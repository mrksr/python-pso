import numpy as np
import pso


def polynom(x):
    return np.linalg.norm(x ** 5 - 6 * x **4 - 26 * x ** 3 + 144 * x ** 2 - 47 * x - 210)


def wiggly(x):
    return np.linalg.norm(x ** 2 * (0.5 + np.sin(x) ** 2))


def test():
    print(pso.minimize(polynom, [(-100, 100)], num_particles=500, verbose=True))
    print(pso.minimize(wiggly, [(-10, 10)] * 5, num_particles=500, verbose=True, max_iter=500))
    print(pso.maximize(wiggly, [(-10, 10)] * 5, num_particles=500, verbose=True, max_iter=500))


if __name__ == "__main__":
    test()
